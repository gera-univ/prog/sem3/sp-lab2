#pragma once

#include <windows.h>


struct MyProcess {
	DWORD pid;
	DWORD parent_id;
	DWORD thread_count;
	DWORD priority_class;
};

struct MyModule {
	WCHAR *name;
	WCHAR *exe_path;
	DWORD base_size;
};

struct MyThread {
	DWORD id;
	DWORD priority;
};

void GetMyProcessList(HANDLE snapshot_handle, struct MyProcess* process_list, size_t* ret_process_count);

void GetMyModuleList(HANDLE snapshot_handle, struct MyModule* module_list, size_t* ret_module_count);

void GetMyThreadList(HANDLE snapshot_handle, DWORD pid, struct MyThread* thread_list, size_t* ret_thread_count);

void FreeMyModuleList(struct MyModule* module_list, size_t module_count);
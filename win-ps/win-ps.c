#include <stdio.h>
#include <stdlib.h>
#include <windows.h>
#include <tlhelp32.h>

#include "../ps-lib/ps-lib.h"

int main(int argc, char** argv) {
	const HANDLE process_snapshot_handle = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

	size_t process_cnt;
	GetMyProcessList(process_snapshot_handle, NULL, &process_cnt);
	if (process_cnt == 0) {
		perror("Incorrect process count");
		exit(1);
	}

	struct MyProcess* process_list = malloc(sizeof(struct MyProcess) * process_cnt);
	GetMyProcessList(process_snapshot_handle, process_list, NULL);
	if (process_list == NULL) {
		perror("Couldn't get process list");
		exit(1);
	}

	printf("User\t\tPID\t\tParent\t\tPriority\t\tModules\n");
	for (size_t i = 0; i < process_cnt; ++i) {
		printf("%lu\t\t%lu\t\t%lu\t\t%lu\n",
		       process_list[i].pid,
		       process_list[i].parent_id,
		       process_list[i].priority_class,
		       process_list[i].thread_count);

		const HANDLE module_snapshot_handle = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, process_list[i].pid);
		const HANDLE thread_snapshot_handle = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD,  0);
		
		size_t module_cnt;
		GetMyModuleList(module_snapshot_handle, NULL, &module_cnt);

		struct MyModule* module_list = malloc(sizeof(struct MyModule) * module_cnt);
		GetMyModuleList(module_snapshot_handle, module_list, NULL);

		for (size_t j = 0; j < module_cnt; ++j) {
			printf("\t\t\t\t\t\t\tName: %ls\n\
\t\t\t\t\t\t\tExecutable: %ls\n\
\t\t\t\t\t\t\tSize: %lu\n\n",
			       module_list[j].name,
			       module_list[j].exe_path, module_list[j].base_size);
		}

		size_t thread_cnt;
		GetMyThreadList(thread_snapshot_handle, process_list[i].pid, NULL, &thread_cnt);

		struct MyThread* thread_list = malloc(sizeof(struct MyThread) * thread_cnt);
		GetMyThreadList(thread_snapshot_handle, process_list[i].pid, thread_list, NULL);

		for (size_t j = 0; j < thread_cnt; ++j) {
			printf("\t\t\t\t\t\t\tName: %lu\n\
\t\t\t\t\t\t\tPriority: %lu\n\n",
			       thread_list[j].id,
			       thread_list[j].priority);
		}

		free(thread_list);
		FreeMyModuleList(module_list, module_cnt);
		CloseHandle(module_snapshot_handle);
		CloseHandle(thread_snapshot_handle);
	}

	free(process_list);
	CloseHandle(process_snapshot_handle);
}

#pragma once
#pragma comment(lib, "comctl32.lib")

#include "list_view.h"

void OnWMCreate(HWND hwnd, HWND* hListView, struct MyProcess** ProcessList, size_t* ProcessListLength);
void OnWMSize(HWND hwnd, HWND hListView);
void OnWMCommand(HWND hwnd, WPARAM wParam, HWND hListView, struct MyProcess** ProcessList, size_t* ProcessListLength);
void OnWMTimer(HWND hwnd, WPARAM wParam, HWND hListView, struct MyProcess** ProcessList, size_t* ProcessListLength);
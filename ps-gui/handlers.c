#include "handlers.h"

#include <commctrl.h>
#include <stdio.h>
#include <tlhelp32.h>

#include "resource.h"
#include "../ps-lib/ps-lib.h"

void FillListView(HWND hListView, struct MyProcess* ProcessList, size_t ProcessListLength) {
	char procID[10];
	char parentID[10];
	char threadCount[10];
	char priorityClass[10];
	for (size_t i = 0; i < ProcessListLength; ++i) {
		sprintf_s(procID, 10, "%d", ProcessList[i].pid);
		sprintf_s(parentID, 10, "%d", ProcessList[i].parent_id);
		sprintf_s(threadCount, 10, "%d", ProcessList[i].thread_count);
		sprintf_s(priorityClass, 10, "%d", ProcessList[i].priority_class);
		LV_CreateNColItem(hListView, 4, procID, parentID, threadCount, priorityClass);
	}
}

void CreateProcessListSnapshot(HWND hwnd, struct MyProcess** ProcessList, size_t* ProcessListLength) {
	HANDLE ProcessListSnapshotHandle = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);

	if (*ProcessList != NULL) {
		free(*ProcessList);
		*ProcessListLength = 0;
	}

	GetMyProcessList(ProcessListSnapshotHandle, NULL, ProcessListLength);
	if (*ProcessListLength == 0) {
		MessageBox(hwnd, "Error", "Incorrect process count.", MB_OK | MB_ICONERROR);
		return;
	}

	*ProcessList = malloc(sizeof(struct MyProcess) * *ProcessListLength);
	GetMyProcessList(ProcessListSnapshotHandle, *ProcessList, NULL);
	if (*ProcessList == NULL) {
		MessageBox(hwnd, "Error", "Couldn't get process list", MB_OK | MB_ICONERROR);
		exit(1);
	}

	CloseHandle(ProcessListSnapshotHandle);
}

void CreateModuleListSnapshot(HWND hwnd, DWORD pid, struct MyModule** ModuleList, size_t* ModuleListLength) {
	HANDLE ListSnapshotHandle = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, pid);

	if (*ModuleList != NULL) {
		free(*ModuleList);
		*ModuleListLength = 0;
	}

	GetMyModuleList(ListSnapshotHandle, NULL, ModuleListLength);
	if (*ModuleListLength == 0) {
		MessageBox(hwnd, "Error", "Incorrect module count.", MB_OK | MB_ICONERROR);
		return;
	}

	*ModuleList = malloc(sizeof(struct MyProcess) * *ModuleListLength);
	GetMyModuleList(ListSnapshotHandle, *ModuleList, NULL);
	if (*ModuleList == NULL) {
		MessageBox(hwnd, "Error", "Couldn't get module list", MB_OK | MB_ICONERROR);
		exit(1);
	}

	CloseHandle(ListSnapshotHandle);
}

void CreateThreadListSnapshot(HWND hwnd, DWORD pid, struct MyThread** ThreadList, size_t* ThreadListLength) {
	HANDLE ThreadListSnapshotHandle = CreateToolhelp32Snapshot(TH32CS_SNAPTHREAD, 0);;

	if (*ThreadList != NULL) {
		free(*ThreadList);
		*ThreadListLength = 0;
	}

	GetMyThreadList(ThreadListSnapshotHandle, pid, NULL, ThreadListLength);

	*ThreadList = malloc(sizeof(struct MyProcess) * *ThreadListLength);
	GetMyThreadList(ThreadListSnapshotHandle, pid, *ThreadList, NULL);
	if (*ThreadList == NULL) {
		MessageBox(hwnd, "Error", "Couldn't get thread list", MB_OK | MB_ICONERROR);
		exit(1);
	}

	CloseHandle(ThreadListSnapshotHandle);
}

void UpdateProcessList(HWND hwnd, HWND hListView, struct MyProcess** ProcessList, size_t* ProcessListLength) {
	CreateProcessListSnapshot(hwnd, ProcessList, ProcessListLength);
	LV_Clear(hListView);
	FillListView(hListView, *ProcessList, *ProcessListLength);
}

void OnWMCreate(HWND hwnd, HWND* hListView, struct MyProcess** ProcessList, size_t* ProcessListLength) {
	*hListView = LV_Create(hwnd, (HMENU)IDC_MAIN_LISTVIEW);
	LV_CreateColumn(*hListView, 0, "PID", 100);
	LV_CreateColumn(*hListView, 1, "Parent ID", 100);
	LV_CreateColumn(*hListView, 2, "Thread Count", 100);
	LV_CreateColumn(*hListView, 3, "Priority Class", 100);

	UpdateProcessList(hwnd, *hListView, ProcessList, ProcessListLength);
}

void OnWMSize(HWND hwnd, HWND hListView) {
	RECT rcClient;
	GetClientRect(hwnd, &rcClient);

	SetWindowPos(hListView, NULL, 0, 0, rcClient.right, rcClient.bottom, SWP_NOZORDER);
}

int GetSelectedProcessID(HWND hListView) {
	char pidText[10];
	int index = LV_GetSelectedItemIndex(hListView);
	LV_GetItemText(hListView, index, 0, (LPSTR*)pidText, 10);
	return atoi(pidText);
}

void AppendString(char* dest, size_t destSize, size_t* offset, const char* format, ...) {
	va_list args;
	va_start(args, format);
	*offset += vsnprintf(dest + *offset, destSize, format, args);
	va_end(args);
}

int CheckNoItemSelected(HWND hwnd, HWND hListView) {
	int index = LV_GetSelectedItemIndex(hListView);

	if (index == -1) {
		MessageBox(hwnd, "No item selected", "Error", MB_OK | MB_ICONERROR);
		return 1;
	}
	return 0;
}

void OnListModules(HWND hwnd, HWND hListView) {
	int pid = GetSelectedProcessID(hListView);
	int index = LV_GetSelectedItemIndex(hListView);

	if (CheckNoItemSelected(hwnd, hListView)) return;

	size_t ModuleListLength = 0;
	struct MyModule* ModuleList = NULL;
	CreateModuleListSnapshot(hListView, pid, &ModuleList, &ModuleListLength);

	const int messageTextSize = 100000;
	char* messageText = malloc(messageTextSize);

	size_t messageTextOffset = 0;
	AppendString(messageText, messageTextSize, &messageTextOffset, "Module list:\n");

	if (ModuleListLength <= 1)
		AppendString(messageText, messageTextSize, &messageTextOffset, "No modules!\n");
	else {
		for (int i = 0; i < ModuleListLength; ++i) {
			AppendString(messageText, messageTextSize, &messageTextOffset, "Module:\t%ls\t%ls\t%lu\n",
			             ModuleList[i].name, ModuleList[i].exe_path, ModuleList[i].base_size);
		}
	}

	MessageBox(hwnd, messageText, "PID", MB_OK | MB_ICONINFORMATION);

	if (ModuleList != NULL) FreeMyModuleList(ModuleList, ModuleListLength);
	if (messageText != NULL) free(messageText);
}

void OnListThreads(HWND hwnd, HWND hListView) {
	int pid = GetSelectedProcessID(hListView);

	if (CheckNoItemSelected(hwnd, hListView)) return;

	size_t ThreadListLength = 0;
	struct MyThread* ThreadList = NULL;
	CreateThreadListSnapshot(hListView, pid, &ThreadList, &ThreadListLength);

	const int messageTextSize = 100000;
	char* messageText = malloc(messageTextSize);

	size_t messageTextOffset = 0;
	AppendString(messageText, messageTextSize, &messageTextOffset, "Module list:\n");

	if (ThreadListLength == 0)
		AppendString(messageText, messageTextSize, &messageTextOffset, "No threads!\n");
	else {
		for (int i = 0; i < ThreadListLength; ++i) {
			AppendString(messageText, messageTextSize, &messageTextOffset, "Thread:\t%lu\t%lu\n",
			             ThreadList[i].id, ThreadList[i].priority);
		}
	}

	MessageBox(hwnd, messageText, "PID", MB_OK | MB_ICONINFORMATION);

	if (ThreadList != NULL) free(ThreadList);
	if (messageText != NULL) free(messageText);
}

void ToggleAutoRefresh(HWND hwnd) {
	HMENU hMenu = GetMenu(hwnd);

	UINT state = GetMenuState(hMenu, ID_FILE_AUTOREFRESH, MF_BYCOMMAND);

	if (state == MF_CHECKED) {
		CheckMenuItem(hMenu, ID_FILE_AUTOREFRESH, MF_UNCHECKED);
	} else if (state == MF_UNCHECKED) {
		CheckMenuItem(hMenu, ID_FILE_AUTOREFRESH, MF_CHECKED);
	}
}

void OnWMCommand(HWND hwnd, WPARAM wParam, HWND hListView, struct MyProcess** ProcessList, size_t* ProcessListLength) {
	switch (LOWORD(wParam)) {
	case ID_FILE_EXIT:
		PostMessage(hwnd, WM_CLOSE, 0, 0);
		break;
	case ID_FILE_CREATESNAPSHOT: {
		UpdateProcessList(hwnd, hListView, ProcessList, ProcessListLength);
	}
	break;
	case ID_ENTRY_LISTMODULES: {
		OnListModules(hwnd, hListView);
	}
	break;
	case ID_ENTRY_LISTTHREADS: {
		OnListThreads(hwnd, hListView);
	}
	break;
	case ID_FILE_AUTOREFRESH: {
		ToggleAutoRefresh(hwnd);
	}
		break;
	}
}

int IsAutoRefreshEnabled(HWND hwnd) {
	HMENU hMenu = GetMenu(hwnd);

	UINT state = GetMenuState(hMenu, ID_FILE_AUTOREFRESH, MF_BYCOMMAND);

	return state == MF_CHECKED;
}

void OnWMTimer(HWND hwnd, WPARAM wParam, HWND hListView, struct MyProcess** ProcessList, size_t* ProcessListLength) {
	switch (LOWORD(wParam)) {
	case IDT_TIMER: {
		if (IsAutoRefreshEnabled(hwnd)) {
			UpdateProcessList(hwnd, hListView, ProcessList, ProcessListLength);
		}
	}
	break;
	}
}

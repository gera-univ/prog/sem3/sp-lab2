#include "list_view.h"

#include "commctrl.h"

HWND LV_Create(HWND hwndParent, HMENU idc) {
	INITCOMMONCONTROLSEX icex; // Structure for control initialization.
	icex.dwICC = ICC_LISTVIEW_CLASSES;
	InitCommonControlsEx(&icex);

	RECT rcClient; // The parent window's client area.

	GetClientRect(hwndParent, &rcClient);

	// Create the list-view window in report view with label editing enabled.
	HWND hListView = CreateWindow(WC_LISTVIEW,
	                              L"",
	                              WS_VISIBLE | WS_CHILD | WS_BORDER | LVS_REPORT |
	                              WS_EX_CLIENTEDGE,
	                              0, 0,
	                              rcClient.right - rcClient.left,
	                              rcClient.bottom - rcClient.top,
	                              hwndParent,
	                              idc,
	                              GetModuleHandle(NULL),
	                              NULL);
	if (hListView == NULL)
		MessageBox(hwndParent, "Could not create list view.", "Error", MB_OK | MB_ICONERROR);

	return (hListView);
}

int LV_CreateColumn(HWND hwndLV, int iCol, char* Text, int width) {
	LVCOLUMN lvc;

	lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;
	lvc.fmt = LVCFMT_LEFT;
	lvc.cx = width;
	lvc.pszText = Text;
	lvc.iSubItem = iCol;
	return ListView_InsertColumn(hwndLV, iCol, &lvc);
}

int LV_CreateItem(HWND hwndList, char* Text) {
	LVITEM lvi = {0};

	lvi.mask = LVIF_TEXT;
	lvi.pszText = Text;
	return ListView_InsertItem(hwndList, &lvi);
}

int LV_CreateNColItem(HWND hwndList, int nColumns, ...) {
	va_list texts;
	va_start(texts, nColumns);

	LVITEM lvi = {0};
	int Ret;

	// Initialize LVITEM members that are common to all items. 
	lvi.mask = LVIF_TEXT;
	lvi.pszText = va_arg(texts, char*);
	Ret = ListView_InsertItem(hwndList, &lvi);
	for (int i = 1; i < nColumns && Ret >= 0; ++i) {
		ListView_SetItemText(hwndList, Ret, i, va_arg(texts, char*));
	}

	va_end(texts);
	return Ret;
}

void LV_Clear(HWND hwndList) {
	ListView_DeleteAllItems(hwndList);
}

int LV_GetSelectedItemIndex(HWND hwndList) {
	return ListView_GetNextItem(hwndList, -1, LVNI_SELECTED);
}

void LV_GetItemText(HWND hwndList, int index, int subItemIndex, LPSTR* buffer, int bufferLength) {
	ListView_GetItemText(hwndList, index, subItemIndex, buffer, bufferLength);
}

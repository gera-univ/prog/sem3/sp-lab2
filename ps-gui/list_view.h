#pragma once
#pragma comment(lib, "comctl32.lib")

#define IDC_MAIN_LISTVIEW	201
#include <windows.h>

HWND LV_Create(HWND hwndParent, HMENU idc);
int LV_CreateColumn(HWND hwndLV, int iCol, char* Text, int width);
int LV_CreateItem(HWND hwndList, char* Text);
void LV_Clear(HWND hwndList);
int LV_CreateNColItem(HWND hwndList, int nColumns, ...);
int LV_GetSelectedItemIndex(HWND hwndList);
void LV_GetItemText(HWND hwndList, int index, int subItemIndex, LPSTR* buffer, int bufferLength);

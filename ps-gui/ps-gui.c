#include <stdio.h>
#include <windows.h>
#include <TlHelp32.h>

#include "handlers.h"
#include "resource.h"
#include "../ps-lib/ps-lib.h"

const char g_szClassName[] = "myWindowClass";
size_t ProcessListLength = 0;
struct MyProcess* ProcessList = NULL;
HWND hListView;

void PerformCleanup() {
	if (ProcessList != NULL) {
		free(ProcessList);
		ProcessList = NULL;
		ProcessListLength = 0;
	}
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam) {
	switch (Message) {
	case WM_CREATE: {
		OnWMCreate(hwnd, &hListView, &ProcessList, &ProcessListLength);
	}
	break;
	case WM_SIZE: {
		OnWMSize(hwnd, hListView);
	}
	break;
	case WM_COMMAND:
		OnWMCommand(hwnd, wParam, hListView, &ProcessList, &ProcessListLength);
		break;
	case WM_CLOSE:
		DestroyWindow(hwnd);
		break;
	case WM_DESTROY:
		PerformCleanup();
		PostQuitMessage(0);
		break;
	case WM_TIMER:
		OnWMTimer(hwnd, wParam, hListView, &ProcessList, &ProcessListLength);
		break;
	default:
		return DefWindowProc(hwnd, Message, wParam, lParam);
	}
	return 0;
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
                   LPSTR lpCmdLine, int nCmdShow) {
	WNDCLASSEX wc;
	HWND hwnd;
	MSG Msg;

	wc.cbSize = sizeof(WNDCLASSEX);
	wc.style = 0;
	wc.lpfnWndProc = WndProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = NULL;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	wc.lpszMenuName = MAKEINTRESOURCE(IDR_MENU);
	wc.lpszClassName = g_szClassName;
	wc.hIconSm = NULL;

	if (!RegisterClassEx(&wc)) {
		MessageBox(NULL, "Window Registration Failed!", "Error!",
		           MB_ICONEXCLAMATION | MB_OK);
		return 0;
	}

	hwnd = CreateWindowEx(
		WS_EX_CLIENTEDGE,
		g_szClassName,
		"Process Viewer",
		WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT, 640, 480,
		NULL, NULL, hInstance, NULL);

	if (hwnd == NULL) {
		MessageBox(NULL, "Window Creation Failed!", "Error!",
		           MB_ICONEXCLAMATION | MB_OK);
		return 0;
	}

	ShowWindow(hwnd, nCmdShow);
	UpdateWindow(hwnd);

	SetTimer(hwnd,
	         IDT_TIMER,
	         5000,
	         (TIMERPROC)NULL);

	while (GetMessage(&Msg, NULL, 0, 0) > 0) {
		TranslateMessage(&Msg);
		DispatchMessage(&Msg);
	}
	return Msg.wParam;
}

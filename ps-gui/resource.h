﻿//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ps-gui.rc
//
#define IDR_MENU                        101
#define IDT_TIMER                       1001
#define ID_FILE_EXIT                    40001
#define ID_ENTRY                        40002
#define ID_ENTRY_DISPLAYINFO            40003
#define ID_FILE_CREATESNAPSHOT          40004
#define ID_ENTRY_LISTTHREADS            40005
#define ID_ENTRY_LISTMODULES            40006
#define ID_FILE_AUTOREFRESH             40008

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40009
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
